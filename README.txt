The Taxtractor module lets you configure a free tagging vocabulary to dispatch values to other fields in the same form.

Taxtractor needs one free tagging vocabulary to be configured before it can be used. If you have many free tagging vocabularies defined, Taxtractor will let you pick one to use as the value dispatcher.

Once you pick a valid vocabulary for the taxtractor, every form using that vocabulary will behave as a Taxtractor, dispatching values read from triple tags (or machine tags as Flickr calls them) to hardcoded fields. Pressing Enter in the vocabulary input form will trigger a submit instead of a preview as usual in Drupal 5. The following tags are preconfigured and depend on their counterpart cck fields being defined on the content type:

key     description                         cck field
------  ----------------------------------  -----------------
t       minutes worked until now            field_punch
c       contract node id                    field_contrat
fact    payable? oui | non                  field_facturable
rt      ticket number from rt.koumbit.org   field_rt



